calendar = {}

def add_activity(date: str, time: str, activity: str):
    if date not in calendar:
        calendar[date] = {}
    calendar[date][time] = activity

def get_time(atime):
    """Devuelve todas las actividades para la hora atime como una lista de tuplas"""
    activities = []
    if atime in calendar[date]:
        for date, times in calendar.items():
            if atime in times:
                activities.append((date, atime, times[atime]))
    return activities

def get_all():
    """Devuelve todas las actividades en el calendario como una lista de tuplas"""
    all_activities = []
    for date, times in calendar.items():
        for time, activity in times.items():
            all_activities.append((date, time, activity))
    return all_activities

def get_busiest():
    """Devuelve la fecha con más actividades, y su número de actividades"""
    busiest_date = ""
    busiest_count = 0
    for date, times in calendar.items():
        if len(times) > busiest_count:
            busiest_date = date
            busiest_count = len(times)
    return busiest_date, busiest_count

def show(activities):
    for (date, time, activity) in activities:
        print(f"{date}. {time}: {activity}")

def check_date(date):
    """Comprueba si una fecha tiene el formato correcto (aaaa-mm-dd)
    Devuelve True o False"""
    # Implementación de la validación de fecha (no implementada aquí)
    return ...

def check_time(time):
    """Comprueba si una hora tiene el formato correcto (hh:mm)
    Devuelve True o False"""
    # Implementación de la validación de hora (no implementada aquí)
    return ...

def get_activity():
    """"Pide al usuario una actividad
    En caso de error al introducirla, vuelve a pedirla desde el principio"""
    # Implementación para capturar y validar la actividad (no implementada aquí)
    return (date, time, activity)

def menu():
    print("A. Introduce actividad")
    print("B. Lista todas las actividades")
    print("C. Día más ocupado")
    print("D. Lista de las actividades de cierta hora dada")
    print("X. Terminar")
    asking = True
    while asking:
        option = input("Opción: ").upper()
        if option in ['A', 'B', 'C', 'D', 'X']:
            asking = False
    return option

def run_option(option):
    """Ejecuta el código necesario para la opción dada"""
    if option == 'A':
        date, time, activity = get_activity()
        add_activity(date, time, activity)
    elif option == 'B':
        all_activities = get_all()
        show(all_activities)
    elif option == 'C':
        busiest_date, busiest_count = get_busiest()
        print(f"La fecha más ocupada es {busiest_date} con {busiest_count} actividades")
    elif option == 'D':
        atime = input("Ingrese la hora (hh:mm): ")
        if check_time(atime):
            time_activities = get_time(atime)
            show(time_activities)

def main():
    proceed = True
    while proceed:
        option = menu()
        if option == 'X':
            proceed = False
        else:
            run_option(option)

if __name__ == "__main__":
    main()
